const express = require('express');
const router = express.Router();

// Require the controllers WHICH WE DID NOT CREATE YET!!
const permissionController = require('../controllers/permission.controller');


// a simple test url to check that all of our files are communicating correctly.
router.get('/', permissionController.getAllPermission);
router.post('/create', permissionController.createPermission);
router.put('/update/:id', permissionController.updatePermission);
router.delete('delete/:id', permissionController.deletePermission);
module.exports = router;
