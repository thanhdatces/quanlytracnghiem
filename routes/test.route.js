const express = require('express');
const router = express.Router();
const verifyToken = require('../_help/jwt.js');

// Require the controllers WHICH WE DID NOT CREATE YET!!
const test = require('../controllers/test.controller');


// a simple test url to check that all of our files are communicating correctly.
router.get('/', verifyToken.verifyToken, test.getAllTest);
router.get('/idAccount/:idAccount', verifyToken.verifyToken, test.getAllTestByIdAccount);
router.post('/mark', verifyToken.verifyToken, test.giveMark);
router.post('/begin', verifyToken.verifyToken, test.getTestByIdClassAndIDTestDetail);
router.post('/create', verifyToken.verifyToken, test.createTest);
module.exports = router;
