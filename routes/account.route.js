const express = require('express');
const router = express.Router();
const verifyToken = require('../_help/jwt.js');

// Require the controllers WHICH WE DID NOT CREATE YET!!
const accountController = require('../controllers/account.controller');


// a simple test url to check that all of our files are communicating correctly.
router.get('/:id', verifyToken.verifyToken, accountController.getAccountById);
router.get('/', verifyToken.verifyToken, accountController.getAllAccount);
router.post('/checkLogin', accountController.checkLogin);
router.post('/create', verifyToken.verifyToken, accountController.createAccount);
router.put('/update/:id', verifyToken.verifyToken, accountController.updateAccount);
router.delete('/delete/:id', verifyToken.verifyToken, accountController.deleteAccount);
module.exports = router;
