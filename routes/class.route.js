const express = require('express');
const router = express.Router();
const verifyToken = require('../_help/jwt.js');

// Require the controllers WHICH WE DID NOT CREATE YET!!
const classController = require('../controllers/class.controller');


// a simple test url to check that all of our files are communicating correctly.
router.get('/', verifyToken.verifyToken, classController.getAllClass);
router.get('/idAccount/:idAccount', verifyToken.verifyToken, classController.getAllClassByIdAccount);
router.post('/create', verifyToken.verifyToken, classController.createClass);
router.put('/update/:id', verifyToken.verifyToken, classController.updateClass);
router.delete('delete/:id', verifyToken.verifyToken, classController.deleteClass);
router.get('/:id', verifyToken.verifyToken, classController.getClassById);
module.exports = router;