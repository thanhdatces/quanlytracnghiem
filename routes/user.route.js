const express = require('express');
const router = express.Router();
const verifyToken = require('../_help/jwt.js');

// Require the controllers WHICH WE DID NOT CREATE YET!!
const user = require('../controllers/user.controller');


// a simple test url to check that all of our files are communicating correctly.
router.get('/:idAccount', verifyToken.verifyToken, user.getUserByIdAccount);
router.post('/update', verifyToken.verifyToken, user.updateUser);

module.exports = router;