const express = require('express');
const router = express.Router();
const verifyToken = require('../_help/jwt.js');

// Require the controllers WHICH WE DID NOT CREATE YET!!
const testDetails = require('../controllers/testDetail.controller');


// a simple test url to check that all of our files are communicating correctly.
router.get('/', testDetails.getAllTestDetail);
router.get('/idSubject/:id', verifyToken.verifyToken, testDetails.getAllTestDetailByIdSubject);
router.get('/:id', testDetails.getTestDetailById);
router.post('/create', verifyToken.verifyToken, testDetails.createTestDetail);
router.delete('/delete/:id', testDetails.deleteTestDetail);
router.put('/update/:id', verifyToken.verifyToken, testDetails.updateTestDetail);

module.exports = router;