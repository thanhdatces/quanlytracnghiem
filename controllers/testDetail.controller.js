const TestDetails = require('../models/testDetail.model');
const Question = require('../models/question.model');
const mongoose = require('../connection')
const db = mongoose.connection;
const jwt = require('jsonwebtoken');
const config = require('../_help/config.json');
var jwtDecode = require('jwt-decode');

//Get All test detail
exports.getAllTestDetail = function (req, res) {
    posts = TestDetails.find({})
    res.send({
        "status": true,
        "data": posts,
        "message": "This is your test details!"
    });
};

//Get test detail By ID
exports.getTestDetailById = function (req, res) {
    posts = TestDetails.findById(req.params.id, function (err, testDetail) {
        if (err) return console.log(err);
    })
    res.send({
        "status": true,
        "data": posts,
        "message": "Test detail by ID!"
    });
};

//Create test detail
exports.createTestDetail = async function (req, res) {
    function getRandomInt(max) {
        return Math.floor(Math.random() * Math.floor(max));
    }
    try {
        if (jwt.verify(req.token, config.secret)) { //all user
            infoUser = (jwtDecode(req.token));
            if (infoUser.sub.IdRole == 1 || infoUser.sub.IdRole == 2) {
                listIdQuestions = [];
                allQuestionOfSubject = await Question.find({ 'idSubject': req.body.idSubject });
                if (allQuestionOfSubject.length < req.body.quantityOfQuestions) {
                    res.send({
                        "status": false,
                        "data": null,
                        "message": "Number of questions is not enough to create this test!"
                    });
                }
                else{
                    for (let i = 0; i < req.body.quantityOfQuestions; i++) {
                        let randomNumber = await getRandomInt(allQuestionOfSubject.length);
                        await listIdQuestions.push(allQuestionOfSubject[randomNumber]._id);
                        allQuestionOfSubject.splice(randomNumber, 1);
                    }
                    let testDetail = new TestDetails({
                        listIdQuestion: listIdQuestions,
                        idSubject: req.body.idSubject,
                        description: req.body.description,
                        isOpen: false,
                        duration: req.body.duration,
                        quantityOfQuestions: req.body.quantityOfQuestions,
                    });
                    testDetail.save(function (err) {
                        if (err) {
                            console.log(err);
                        }
                        res.send({
                            "status": true,
                            "data": null,
                            "message": "Test detail is created!"
                        });
                    });    
                }      
            }
            else {
                res.send({
                    "status": false,
                    "data": null,
                    "message": "You are not permitted!"
                });
            }
        }
    }
    catch (e) {
        res.send({
            "status": false,
            "data": e,
            "message": "Unidentified error!"
        });
    }
}

//Update test detail ( chua xai dc dau :v )
exports.updateTestDetail = async function (req, res) {
    try {
        if (jwt.verify(req.token, config.secret)) { //all user
            infoUser = (jwtDecode(req.token));
            if (infoUser.sub.IdRole == 1 || infoUser.sub.IdRole == 2) {
                await TestDetails.findByIdAndUpdate(req.params.id,
                    {
                        description: req.body.description,
                        isOpen: req.body.isOpen,
                        duration: req.body.duration,
                    });
                res.send({
                    "status": true,
                    "data": null,
                    "message": "Success!"
                });
            }
            else {
                res.send({
                    "status": false,
                    "data": null,
                    "message": "You are not permitted!"
                });
            }
        }
    }
    catch (e) {
        console.log(e)
        res.send({
            "status": false,
            "data": e,
            "message": "Unidentified error!"
        });
    }
}

//Delete test detail By ID
exports.deleteTestDetail = async function (req, res) {
    await TestDetails.findByIdAndDelete(req.params.id, function (err, testDetail) {
        if (err) return console.log(err);
        res.send({
            "status": true,
            "data": null,
            "message": "Test details is deleted!"
        });
    })
};

exports.getAllTestDetailByIdSubject = async function (req, res) {
    try {
        if (jwt.verify(req.token, config.secret)) { //all user
            infoUser = (jwtDecode(req.token));
            posts = await TestDetails.find({ 'idSubject': req.params.id }).select('-listIdQuestion');
            res.send({
                "status": true,
                "data": posts,
                "message": "!"
            });
        }
    }
    catch (e) {
        res.send({
            "status": false,
            "data": e,
            "message": "Unidentified error!"
        });
    }
}