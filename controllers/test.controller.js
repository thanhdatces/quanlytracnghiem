const Test = require('../models/test.model');
const mongoose = require('../connection')
const db = mongoose.connection;
const TestDetails = require('../models/testDetail.model');
const Class = require('../models/class.model');
const Question = require('../models/question.model');
const Answer = require('../models/answer.model');
const jwt = require('jsonwebtoken');
const config = require('../_help/config.json');
var jwtDecode = require('jwt-decode');


function shuffle(array) {
    function getRandomInt(max) {
        return Math.floor(Math.random() * Math.floor(max));
    }
    for (let i = 0; i < array.length; i++) {
        let randomNumber = getRandomInt(array.length);
        let tmp = array[i];
        array[i] = array[randomNumber];
        array[randomNumber] = tmp;
    }
}
//Simple version, without validation or sanitation
exports.getTestByIdClassAndIDTestDetail = async function (req, res) {
    try {
        if (jwt.verify(req.token, config.secret)) {
            infoUser = (jwtDecode(req.token));
            testDetail = await TestDetails.findById(req.body.idTestDetail);
            if (testDetail.isOpen) {
                idClass = await Class.findOne({ 'idAccount_Subject.idAccount': req.body.idAccount, 'idAccount_Subject.idSubject': req.body.idSubject });
                findTest = await Test.findOne({ 'idClass': idClass._id, 'idTestDetail': req.body.idTestDetail });
                if (!findTest.isComplete) {
                    listIdQuestion = testDetail.listIdQuestion;
                    displayedQuestion = [];
                    shuffle(listIdQuestion);
                    for (let i = 0; i < listIdQuestion.length; i++) {
                        let question = await Question.findById(listIdQuestion[i]).populate('listIdAnswer')
                        shuffle(question.listIdAnswer);
                        displayedQuestion.push({
                            '_id': question._id,
                            'content': question.content,
                            'listIdAnswer': question.listIdAnswer,
                        })
                    }
                    res.send({
                        "status": true,
                        "data": {
                            "questions": displayedQuestion,
                            "idClass": idClass._id,
                            "duration": testDetail.duration,
                        },
                        "message": "How to message this!",
                    });
                }
                else {
                    res.send({
                        "status": false,
                        "data": '',
                        "message": "You did this test, relax!",
                    });
                }

            }
            else {
                res.send({
                    "status": false,
                    "data": '',
                    "message": "The test is not open yet!",
                });
            }
        }
    }
    catch (e) {
        console.log(e)
        res.send({
            "status": false,
            "data": e,
            "message": "Unidentified error!"
        });
    }
};

exports.giveMark = async function (req, res) {
    try {
        if (jwt.verify(req.token, config.secret)) {
            infoUser = (jwtDecode(req.token));
            posts = [];
            mark = 0;
            for (let questionThis of req.body.data) {
                if (questionThis.idAnswer !== null) {
                    questionThat = await Question.findOne({ '_id': questionThis.idQuestion });
                    if (questionThis.idAnswer == questionThat.idCorrectAnswer) {
                        mark = mark + 1;
                    }
                    posts.push({
                        'selectedAnswer': questionThis.idAnswer,
                        'correctAnswer': questionThat.idCorrectAnswer,
                    });
                }
                else {
                    questionThat = await Question.findOne({ '_id': questionThis.idQuestion });
                    posts.push({
                        'selectedAnswer': questionThis.idAnswer,
                        'correctAnswer': questionThat.idCorrectAnswer,
                    });
                }
            }
            finalMark = mark / req.body.count * 10;
            findTest = await Test.findOne({ 'idClass': req.body.idClass, 'idTestDetail': req.body.idTestDetail });
            await Test.findByIdAndUpdate(findTest._id,
                {
                    result: Number(finalMark),
                    timeComplete: req.body.timeComplete,
                    isComplete: true,
                });
            data = {
                'posts': posts,
                'mark': finalMark
            }
            res.send({
                "status": true,
                "data": data,
                "message": "Your result!"
            });
        }
    }
    catch (e) {
        res.send({
            "status": false,
            "data": e,
            "message": "Unidentified error!"
        });
    }
};

//get all test
exports.getAllTest = function (req, res) {

};

//get all test by id Account
exports.getAllTestByIdAccount = async function (req, res) {
    try {
        if (jwt.verify(req.token, config.secret)) { //all user
            infoUser = (jwtDecode(req.token));
            posts = [];
            classA = await Class.find({ 'idAccount_Subject.idAccount': req.params.idAccount }).populate('idAccount_Subject.idSubject');
            for (classB of classA) {
                test = await Test.find({ 'idClass': classB._id }).populate('idTestDetail');
                posts.push(test);
            }
            res.send({
                status: true,
                data: {
                    posts: posts,
                    class: classA,
                },
                message: 'Test history'
            })
        }
    }
    catch (e) {
        console.log(e)
        res.send({
            "status": false,
            "data": e,
            "message": "Unidentified error!"
        });
    }
};
exports.createTest = async function (req, res) {
    /*let idClass = await Class.find();
    let idTestDetail = await TestDetails.find();
    for (let i = 0; i< idClass.length; i++) {
        for (let j = 0; j < idTestDetail.length; j++) {
            let test = new Test({
                idClass: idClass[i]._id,
                idTestDetail: idTestDetail[j]._id,
                result: 0,
                timeComplete: 0,
            });
            test.save(function (err) {
                if (err) {
                    console.log(err);
                }
                console.log('success')
            });
        }
    }
    res.send('Test Created successfully');*/
    let test = new Test({
        idClass: req.body.idclass,
        idTestDetail: req.body.idtestdetail,
        result: 0,
        timeComplete: 0,
    });
    test.save();
};
