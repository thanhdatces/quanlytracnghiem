const Account = require('../models/account.model');
const db = require('../connection').connection;
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const config = require('../_help/config.json');
var jwtDecode = require('jwt-decode');
const User = require('../models/user.model');
//Create Question
exports.createAccount = async function (req, res) {
    try {
        if (jwt.verify(req.token, config.secret)) {
            infoUser = (jwtDecode(req.token));
            if (infoUser.sub.IdRole == 1) { // cho admin
                check = await Account.find({ UserName: req.body.UserName });
                if (check.length < 1) {
                    if (req.body.Password !== '' && req.body.UserName !== '' && req.body.IdRole !== '') {
                        let account = new Account(
                            {
                                UserName: req.body.UserName,
                                Password: bcrypt.hashSync(req.body.Password, 10),
                                Facebook: req.body.Facebook,
                                Gmail: req.body.Gmail,
                                IdRole: req.body.IdRole
                            });
                        account.save(async function (err) {
                            if (err) {
                                console.log(err);
                            }
                            else{
                                account = await Account.findOne({'UserName': account.UserName});
                                let user = new User({
                                    idAccount: account._id,
                                })
                                user.save();
                            }
                            res.send({
                                "status": true,
                                "data": null,
                                "message": "Account created",
                            })
                        });
                    }
                    else {
                        res.send({
                            "status": false,
                            "data": null,
                            "message": "Please fill all the required fields",
                        })
                    }
                }
                else {
                    res.send({
                        "status": false,
                        "data": '',
                        "message": "Username is already existed!"
                    });
                }
            }
        }
        else {
            res.send({
                "status": false,
                "data": '',
                "message": "You are not permitted!"
            });
        }
    }
    catch (e) {
        res.send({
            "status": false,
            "data": e,
            "message": "Unidentified error!"
        });
    }
};

//Get by Id
exports.getAccountById = async function (req, res) {
    try {
        if (jwt.verify(req.token, config.secret)) {
            infoUser = (jwtDecode(req.token));
            if (infoUser.sub.IdRole == 1) { // cho admin
                posts = await Account.findById(req.params.id).select('-Password');
                res.send({
                    "status": true,
                    "data": posts,
                    "message": "Get all account",
                });
            }
        }
        else {
            res.send({
                "status": false,
                "data": '',
                "message": "You are not permitted!"
            });
        }
    }
    catch (e) {
        res.send({
            "status": false,
            "data": e,
            "message": "Unidentified error!"
        });
    }
};

//login
exports.checkLogin = async function (req, res) {
    try {
        idUser = await Account.findOne({ UserName: req.body.UserName });
        if (idUser && bcrypt.compareSync(req.body.Password, idUser.Password)) {
            console.log('login')
            const infoUser = await Account.findOne({ UserName: req.body.UserName }).select('-Password');
            const token = jwt.sign({ sub: infoUser }, config.secret); 
            user = await User.findOne({'idAccount': infoUser._id});
            profilePicture = user.profilePicture;
            res.send({
                "status": true,
                "data": infoUser,
                "profilePicture": profilePicture,
                "token": token,
                "message": "Successfully log-in",
            });
        }
        else {
            res.send({
                "status": false,
                "data": null,
                "token": null,
                "message": "Account or password is wrong! Please check again."
            });
        }
    }
    catch (e) {
        console.log(e)
        res.send({
            "status": false,
            "data": e,
            "message": "Unidentified error!"
        });
    }
};

//Get All ACcount
exports.getAllAccount = async function (req, res) {
    try {
        if (jwt.verify(req.token, config.secret)) {
            infoUser = (jwtDecode(req.token));
            if (infoUser.sub.IdRole == 1) { // cho admin
                posts = await Account.find().select('-Password')
                res.send({
                    "status": true,
                    "data": posts,
                    "message": "Find all account!"
                });
            }
            else {
                res.send({
                    "status": false,
                    "data": '',
                    "message": "You are not permitted!"
                });
            }
        }
    }
    catch (e) {
        console.log(e)
        res.send({
            "status": false,
            "data": e,
            "message": "Unidentified error!"
        });
    }
};

//Update
exports.updateAccount = async function (req, res) {    
    try {
        if (jwt.verify(req.token, config.secret)) {
            infoUser = (jwtDecode(req.token));
            if (infoUser.sub.IdRole == 1) { // cho admin
                console.log(req.body)
                await Account.findByIdAndUpdate(req.params.id, req.body)
                res.send({
                    "status": true,
                    "data": '',
                    "message": 'Updated'
                })
            }
            else {
                res.send({
                    "status": false,
                    "data": '',
                    "message": "You are not permitted!"
                });
            }
        }
    }
    catch (e) {
        console.log(e)
        res.send({
            "status": false,
            "data": e,
            "message": "Unidentified error!"
        });
    }
};

//Delete
exports.deleteAccount = async function (req, res) {
    try {
        if (jwt.verify(req.token, config.secret)) {
            infoUser = (jwtDecode(req.token));
            if (infoUser.sub.IdRole == 1) { // cho admin
                await Account.findByIdAndDelete(req.params.id)
                res.send({
                    "status": true,
                    "data": '',
                    "message": 'Deleted'
                })
            }
            else {
                res.send({
                    "status": false,
                    "data": '',
                    "message": "You are not permitted!"
                });
            }
        }
    }
    catch (e) {
        console.log(e)
        res.send({
            "status": false,
            "data": e,
            "message": "Unidentified error!"
        });
    }
};
