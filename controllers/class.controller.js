const Class = require('../models/class.model');
// const Account = require('../models/account.model');
// const Subject = require('../models/subject.model');
// const db = require('../connection').connection;
const jwt = require('jsonwebtoken');
const config = require('../_help/config.json');
var jwtDecode = require('jwt-decode');
var Test = require('../models/test.model');
var TestDetail = require('../models/testDetail.model');

//Create Question
exports.createClass = async function (req, res) {
    // let idAccount = await Account.find();
    // let idSubject = await Subject.find();
    // for (let i = 0; i< idAccount.length; i++) {
    //     for (let j = 0; j < idSubject.length; j++) {
    //         let newClass = await new Class(
    //             {
    //                 idAccount_Subject: {
    //                     idAccount: idAccount[i]._id,
    //                     idSubject: idSubject[j]._id
    //                 }
    //             });
    //         console.log(newClass.IdAccount_Subject);
    //         newClass.save(function (err) {
    //             if (err) {
    //                 console.log(err);
    //             }
    //             else
    //                 console.log('Class Created successfully');
    //         });
    //     }
    // }
    // res.send('Class Created successfully')
    try {
        if (jwt.verify(req.token, config.secret)) {
            infoUser = (jwtDecode(req.token)); //all user
            let newClass = new Class(
                {
                    idAccount_Subject: {
                        idAccount: req.body.idAccount,
                        idSubject: req.body.idSubject
                    }
                });
            a = await newClass.save();
            console.log(a._id)
            testDetails = await TestDetail.find({idSubject: req.body.idSubject})
            for( t of testDetails){
                let test = await new Test({
                    'idClass': a._id,
                    'idTestDetail': t._id,
                    'result': 0,
                    'timeComplete': 0,
                    'isComplete': false,
                });
                b = await test.save();
            }
            console.log(b)
            res.send({
                "status": true,
                "data": null,
                "message": "You joined this class!"
            });
        }
    }
    catch (e) {
        res.send({
            "status": false,
            "data": e,
            "message": "Unidentified error!"
        });
    }
};

//Get All Class by id account
exports.getAllClassByIdAccount = async function (req, res) {
    try {
        if (jwt.verify(req.token, config.secret)) { //all user
            infoUser = (jwtDecode(req.token));
            console.log(infoUser.sub.IdRole);
            posts = await Class.find({ 'idAccount_Subject.idAccount': req.params.idAccount }).populate('idAccount_Subject.idSubject');
            res.send({
                "status": true,
                "data": posts,
                "message": "!"
            });
        }
    }
    catch (e) {
        res.send({
            "status": false,
            "data": e,
            "message": "Unidentified error!"
        });
    }
};

//Get All Class
exports.getAllClass = async function (req, res) {
    try {
        if (jwt.verify(req.token, config.secret)) {
            infoUser = (jwtDecode(req.token));
            if (infoUser.sub.IdRole == 1) { //admin
                posts = await Class.find({})
                res.send({
                    "status": true,
                    "data": posts,
                    "message": "!"
                });
            }
            else {
                res.send({
                    "status": false,
                    "data": null,
                    "message": "You have no right to access!"
                });
            }
        }
    }
    catch (e) {
        res.send({
            "status": false,
            "data": e,
            "message": "Unidentified error!"
        });
    }
};

//get one class
exports.getClassById = async function (req, res) {
    try {
        if (jwt.verify(req.token, config.secret)) {
            infoUser = (jwtDecode(req.token));
            if (infoUser.sub.IdRole == 1) { // cho admin
                posts = await Class.findById(req.params.id, function (err, returnclass) {
                    if (err) return console.log(err);
                }).populate('IdAccount_Subject.IdAccount').populate('IdAccount_Subject.IdSubject')
                res.send({
                    "status": true,
                    "data": posts,
                    "message": "!"
                });
            }
            else {
                res.send({
                    "status": false,
                    "data": e,
                    "message": "You are not permitted!"
                });
            }
        }
    }
    catch (e) {
        res.send({
            "status": false,
            "data": e,
            "message": "Unidentified error!"
        });
    }
};

//Update
exports.updateClass = function (req, res) {
};

//Delete
exports.deleteClass = function (req, res) {
};
