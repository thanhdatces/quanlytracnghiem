const Question = require('../models/question.model');
const Answer = require('../models/answer.model');
const db = require('../connection').connection;
const config = require('../_help/config.json');
const jwt = require('jsonwebtoken');
var jwtDecode = require('jwt-decode');
const TestDetail = require('../models/testDetail.model');

//Create Question
exports.createQuestion = async function (req, res) {
    try {
        let empty = 0;
        let ok = 0;
        if (jwt.verify(req.token, config.secret)) {
            infoUser = (jwtDecode(req.token));
            if (infoUser.sub.IdRole == 2 || infoUser.sub.IdRole == 1) { // cho teacher
                questionsArray = req.body.questions;
                for (let questionsA of questionsArray) {
                    if (questionsA.content == ''
                        || questionsA.listIdAnswer[0].content == undefined || questionsA.listIdAnswer[1].content == undefined
                        || questionsA.listIdAnswer[2].content == undefined || questionsA.listIdAnswer[3].content == undefined) {
                        empty = empty + 1;
                    }
                    else {
                        ok = ok + 1;
                        db.collection('answers').insertMany(questionsA.listIdAnswer, function (err, result) {
                            let answersMap = Object.values(result.insertedIds);
                            let correctAnswer = answersMap[3];
                            let question = new Question(
                                {
                                    content: questionsA.content,
                                    idSubject: req.body.idSubject,
                                    listIdAnswer: answersMap,
                                    idCorrectAnswer: correctAnswer,
                                });
                            question.save();
                        });
                    }
                }
                if (empty !== 0) {
                    res.send({
                        "status": true,
                        "data": null,
                        "message": "" + ok + " questions are create!" + empty + " questions failed"
                    });
                }
                else {
                    res.send({
                        "status": true,
                        "data": null,
                        "message": "All questions are created!"
                    });
                }
            }
            else {
                res.send({
                    "status": false,
                    "data": null,
                    "message": "You are not permitted!"
                });
            }
        }
    }
    catch (e) {
        console.log(e)
        res.send({
            "status": false,
            "data": e,
            "message": "Unidentified error!"
        });
    }
};

//Delete Question By ID
exports.deleteQuestion = async function (req, res) {
    try {
        if (jwt.verify(req.token, config.secret)) {
            infoUser = (jwtDecode(req.token));
            if (infoUser.sub.IdRole == 2 || infoUser.sub.IdRole == 1) { // cho teacher
                let idAnswers = await Question.findById(req.params.id);
                await Answer.deleteMany({ _id: idAnswers.listIdAnswer });
                await Question.findByIdAndDelete(req.params.id, function (err, question) {
                    if (err) return console.log(err);
                    res.send({
                        "status": true,
                        "data": question,
                        "message": "Question is deleted!"
                    });
                });
            }
            else {
                res.send({
                    "status": false,
                    "data": '',
                    "message": "You are not permitted!"
                });
            }
        }
    }
    catch (e) {
        res.send({
            "status": false,
            "data": e,
            "message": "Unidentified error!"
        });
    }
};

//Update Question By ID
exports.updateQuestion = async function (req, res) {
    try {
        if (jwt.verify(req.token, config.secret)) {
            infoUser = (jwtDecode(req.token));
            if (infoUser.sub.IdRole == 2 || infoUser.sub.IdRole == 1) { // cho teacher
                let newAnswer = req.body.listIdAnswer;
                let idAnswers = await Question.findById(req.params.id);
                let mangAnswers = [];
                for (let i = 0; i < idAnswers.listIdAnswer.length; i++) {
                    let result = await Answer.findByIdAndUpdate(idAnswers.listIdAnswer[i],
                        {
                            content: newAnswer[i].content,//value of answer content
                        });
                    mangAnswers.push(result._id);
                }
                await Question.findByIdAndUpdate(req.params.id,
                    {
                        content: req.body.content, //question content
                        idSubject: req.body.idSubject._id,
                        listIdAnswer: mangAnswers
                    });
                res.send({
                    "status": true,
                    "data": null,
                    "message": "Your question is updated!"
                });
            }
            else {
                res.send({
                    "status": false,
                    "data": '',
                    "message": "You are not permitted!"
                });
            }
        }
    }
    catch (e) {
        res.send({
            "status": false,
            "data": e,
            "message": "Unidentified error!"
        });
    }
};

//Get Question By ID
exports.getQuestionById = async function (req, res) {
    try {
        if (jwt.verify(req.token, config.secret)) {
            infoUser = (jwtDecode(req.token));
            if (infoUser.sub.IdRole == 2 || infoUser.sub.IdRole == 1) { // cho teacher
                posts = await Question.findById(req.params.id).populate('listIdAnswer').populate('idSubject')
                res.send({
                    "status": true,
                    "data": posts,
                    "message": "Get question by id!"
                });
            }
            else {
                res.send({
                    "status": false,
                    "data": '',
                    "message": "You are not permitted!"
                });
            }
        }
    }
    catch (e) {
        res.send({
            "status": false,
            "data": e,
            "message": "Unidentified error!"
        });
    }
};

//Get Question By ID Subject
exports.getQuestionByIdSubject = async function (req, res) {
    try {
        if (jwt.verify(req.token, config.secret)) {
            infoUser = (jwtDecode(req.token));
            if (infoUser.sub.IdRole == 2 || infoUser.sub.IdRole == 1) { // cho teacher
                posts = await Question.find({ idSubject: req.params.idSubject }).populate('listIdAnswer').populate('idSubject').populate('idCorrectAnswer');
                if (posts.length < 1) {
                    res.send({
                        "status": false,
                        "data": [],
                        "message": "Subject has no question!"
                    });
                }
                else {
                    res.send({
                        "status": true,
                        "data": posts,
                        "message": "Get question by id subject!"
                    });
                }
            }
            else {
                res.send({
                    "status": false,
                    "data": '',
                    "message": "You are not permitted!"
                });
            }
        }
    }
    catch (e) {
        res.send({
            "status": false,
            "data": e,
            "message": "Unidentified error!"
        });
    }
};

//Get Question By ID Subject
exports.getQuestionByIdTestDetail = async function (req, res) {
    try {
        if (jwt.verify(req.token, config.secret)) {
            infoUser = (jwtDecode(req.token));
            if (infoUser.sub.IdRole == 2 || infoUser.sub.IdRole == 1) { // cho teacher
                posts = [];
                testDetailThis = await TestDetail.findOne({ _id: req.params.idTestDetail })
                if (testDetailThis.length < 1) {
                    res.send({
                        "status": false,
                        "data": [],
                        "message": "TestDetail is empty!"
                    });
                }
                else {
                    questionThis = await Question.find({ 'idSubject': testDetailThis.idSubject }).populate('listIdAnswer')
                    questionThis.some(r => {
                        if (testDetailThis.listIdQuestion.indexOf(r._id) >= 0){
                            posts.push(r)
                        }
                    })
                    res.send({
                        "status": true,
                        "data": posts,
                        "message": "Get question by id subject!"
                    });
                }
            }
            else {
                res.send({
                    "status": false,
                    "data": '',
                    "message": "You are not permitted!"
                });
            }
        }
    }
    catch (e) {
        res.send({
            "status": false,
            "data": e,
            "message": "Unidentified error!"
        });
    }
};

//Get All Question
exports.getAllQuestion = async function (req, res) {
};
