const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let QuestionSchema = new Schema({
    content: {
        type: String
    },
    idSubject: {
        type: mongoose.Schema.ObjectId,
        ref: 'Subject'
    },
    listIdAnswer: [{
        type: mongoose.Schema.ObjectId,
        ref: 'Answer'
    }],
    idCorrectAnswer: {
        type: mongoose.Schema.ObjectId,
        ref: 'Answer'
    }
});


// Export the model
module.exports = mongoose.model('Question', QuestionSchema);
