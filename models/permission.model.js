const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let PermissionSchema = new Schema({
    idRole: {
        type:  mongoose.Schema.ObjectId,
        ref: 'Role',
        require: true
    },
    function: {
        type: String,
        require: true
    },
    isEnable: {
        type: Boolean,
        require: true
    }
});


// Export the model
module.exports = mongoose.model('Permission', PermissionSchema);
