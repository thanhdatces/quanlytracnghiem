const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const db = require('../connection').connection;

let ClassSchema = new Schema({
    idAccount_Subject: {
        idAccount: {
            type:  mongoose.Schema.ObjectId,
            ref: 'Account',
        },
        idSubject: {
            type:  mongoose.Schema.ObjectId,
            ref: 'Subject'
        },
    }
});

ClassSchema.index({ idAccount: 1, idSubject: 1 }, { unique: true })

// Export the model
module.exports = mongoose.model('Class', ClassSchema);
