const express = require('express');
const bodyParser = require('body-parser');
// initialize our express app
const app = express();
app.use(bodyParser.json({limit: '50mb', extended: true}))
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}))
const mongoose = require('./connection');
const answer = require('./routes/answer.route');
const question = require('./routes/question.route');
const subject = require('./routes/subject.route');
const testDetails = require('./routes/testDetail.route');
const account = require('./routes/account.route');
const role = require('./routes/role.route');
const test = require('./routes/test.route');
const permission = require('./routes/permission.route');
const classRoute = require('./routes/class.route');
const user = require('./routes/user.route');
//const jwt = require('./_help/jwt');
let port = process.env.PORT || 1234;

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
app.use(function (req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Credentials", "true");
    res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");
    res.setHeader("Access-Control-Allow-Headers", "Authorization, Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    next();
});
app.use('/answer', answer);
app.use('/question', question);
app.use('/subject', subject);
app.use('/testDetails', testDetails);
app.use('/account', account);
app.use('/role', role);
app.use('/test', test);
app.use('/permission', permission);
app.use('/class', classRoute);
app.use('/user', user);

app.listen(port, () => {
    console.log('Server is up and running on port number ' + port);
});
